#!/bin/bash
echo "Enter the bill amount: "
read bill
if [ $bill -lt 0 ]
then
	echo "Invalid bill amount: $bill"
elif [ $bill -gt 999 ]
then
	bill=$(($bill-100))
	echo "Your final bill is: $bill"
else
	bill=$(($bill-50))
	echo "Your final bill is: $bill"
fi

