#!/bin/bash
word=cat words.txt | head -1
echo "$word"
echo "What is the first position letter you need"
read pos1
echo "What is the second position letter you need"
read pos2
echo "Your word to guess is:"
total_char=${#word}
echo $total_char
for((i=0; i<$total_char; i++))
do
#	if [ $pos1 -ne 0 
	if [ $i -eq $pos1 -o $i -eq $pos2 ]
	then
		echo -n "_ "	
	else	
		char=${word:i:1}
		echo -n "$char "
	fi
done
