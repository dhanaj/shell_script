#!/bin/bash
DAY="$(date +%Y_%m_%d)"
fname=$DAY-backup.tar.gz
echo "$fname"
#if [ test `$find . -name fname.tar.gz) ]
#if [ $(find . -name *.tar.gz) ]
#test -f $fname
#num=$?
#echo "$num"
if [ -f $fname ]
then
	echo "Today's Backup already exists"
	exit
else
	BACKUP="/home/dhana/data_files/$fname"
	tar -czf $BACKUP /home/dhana/data_files
	echo "Today's Backup created successfully"
	echo "Backup completed at `date`"
fi

